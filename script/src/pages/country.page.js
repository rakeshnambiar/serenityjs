'use strict';

const countryDetailsPage = function() {

    const po = this;

    po.Page_title = by.css('h1');
    po.Country_code = by.css('input');
    po.Country_name = by.css('textarea');

    po.openApp = async() => {
        await browser.get('/react-graphql-jest');
        await browser.sleep(1000);
    };

    po.getPageTitle = async() => {
      try{
          return browser.element(po.Page_title).getText();
      }catch (e) {
          throw new Error('ERROR: While getting the page title');
      }
    };

    po.getCountryCode = async() => {
      try{
          return browser.element(po.Country_code).getAttribute('value');
      }catch (e) {
          throw new Error('ERROR: While getting the Country code');
      }
    };

    po.getCountryName = async() => {
        try{
            return browser.element(po.Country_name).getText();
        }catch (e) {
            throw new Error('ERROR: While getting the Country name');
        }
    };

    po.setCountryCode = async(code) => {
      try{
          let country_code_field = browser.element(po.Country_code);
           await country_code_field.clear();
           await country_code_field.sendKeys(code);
          await browser.sleep(1000);
      }catch (e) {
          throw new Error('ERROR: While entering the Country code - ' + code + 'error - ' + e);
      }
    };


};

module.exports = new countryDetailsPage();