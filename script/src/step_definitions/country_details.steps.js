'use strict';
const
    detailsPage = require('../pages/country.page'),
    assert = require('../utils/assert').assert,
    expect = require('../utils/expect').expect;

module.exports = function countryDetailsSteps() {
    this.setDefaultTimeout(60 * 1000);
    browser.ignoreSynchronization = true;
    browser.waitForAngular(false);

    this.Given(/^I am a User$/, async () => {
        await detailsPage.openApp();
    });


    this.When(/^I open the react single page Website$/, async() => {
        expect(await browser.getCurrentUrl()).include('react-graphql-jest');
    });

    this.Then(/^I should see the page title "([^"]*)"$/, async(title) =>{
        assert.isTrue(await detailsPage.getPageTitle() === title, 'FAILED: Page title validation');
    });


    this.Then(/^I should be able to see the default country code "([^"]*)" on the page$/, async(def_code) => {
        assert.isTrue(await detailsPage.getCountryCode() === def_code, 'FAILED: Default country code validation');
    });

    this.Then(/^the country name "([^"]*)" should also available on the following text\-area field$/, async(def_countryName) => {
        assert.isTrue(await detailsPage.getCountryName() === def_countryName, 'FAILED: Default country name validation');
    });

    this.When(/^I enter the "([^"]*)"$/, async (code) => {
        await detailsPage.setCountryCode(code);
    });

    this.Then(/^I should be able to see the matching "([^"]*)" populated on the screen$/, async (country_name) => {
        assert.isTrue(await await detailsPage.getCountryName() === country_name, 'FAILED - Country name NOT matching - ' + country_name)
    });
};