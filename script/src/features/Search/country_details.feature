Feature: Verify the accuracy of Country details on the React-GraphQL single page application

  In order to get the Country name
  As as user
  I should have the provision to enter the country code

  @TC01_RegressionTest   @RegressionTest
  Scenario: To verify the default page title
    Given I am a User
    When I open the react single page Website
    Then I should see the page title "Country Details - CI/CD"


#   @TC02_RegressionTest   @RegressionTest
#   Scenario: To verify the accuracy of country code and country name for the default value
#     Given I am a User
#     When I open the react single page Website
#     Then I should be able to see the default country code "US" on the page
#     And the country name "United States" should also available on the following text-area field

#   @TC03_RegressionTest   @RegressionTest
#   Scenario Outline: To verify the accuracy of Country details
#     Given I am a User
#     When I enter the "<country_code>"
#     Then I should be able to see the matching "<country_name>" populated on the screen

#     Examples:
#       | country_code | country_name   |
#       | IN           | India          |
#       | GB           | United Kingdom |
#       | CA           | Canada         |
